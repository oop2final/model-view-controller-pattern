##Authors
+ TAWEERAT CHAIMAN 5710546259
+ PATINYA YONGYAI 5710547204

## Model-View-Controller Pattern
---

Model-View-Controller pattern is help to seperate the application view (UI) from the logic.

They are consist of three parts 

* `Model` (Contain the logic and state of application).
* `View` (To show the appearance of data from Model to user).
* `Controller` (The interconnect between Model and View to handles the user's interaction with the View and the Model).

![gBfe7.jpg](https://bitbucket.org/repo/69LGRo/images/68509917-gBfe7.jpg)

1. When user is do something on GUI(View), the GUI will send the command to controller 
2. The controller will ask the model to update the data.
3. The controller maybe also ask the view the change appearance.
4. The model will notify the GUI(View) >> "I have been updated".
> We can add view as the observer of model by using Observer pattern
5. The GUI will get state from model.


>If we want to change the logic, just change the model and it isn't effect the view. This allow us to change the new logic many time by use the same GUI.

We have an example with source code. See below.

[Model.java](https://bitbucket.org/oop2final/model-view-controller-pattern/src/d6db91aa1cc06748f1a25351c93e4600c213009f/src/Model.java)

[Cinema.java (Model concrete)](https://bitbucket.org/oop2final/model-view-controller-pattern/src/deb8dbb734a85c28771a08998ccbaf659430eabd/src/Cinema.java)

[GUIHandler.java](https://bitbucket.org/oop2final/model-view-controller-pattern/src/deb8dbb734a85c28771a08998ccbaf659430eabd/src/GUIHandler.java)

[Controller.java](https://bitbucket.org/oop2final/model-view-controller-pattern/src/deb8dbb734a85c28771a08998ccbaf659430eabd/src/Controller.java)

[GUI.java (View)](https://bitbucket.org/oop2final/model-view-controller-pattern/src/deb8dbb734a85c28771a08998ccbaf659430eabd/src/GUI.java)

[Main.java](https://bitbucket.org/oop2final/model-view-controller-pattern/src/d6db91aa1cc06748f1a25351c93e4600c213009f/src/Main.java)

![screenshot.png](https://bitbucket.org/repo/69LGRo/images/26601298-screenshot.png)

> This is the simple program for reserve the cinema seat when click on seat, if it available it will change to red color.

From example, The controller has either View and Model. And View is initial this event to JLabel.
```
class BookingEvent extends MouseAdapter{
		private JLabel label;
		public BookingEvent(JLabel label){
			this.label = label;
		}
		
		public void mousePressed(MouseEvent e){
			String seatNO = label.getText();
			if(guiHandler.bookSeat(seatNO)){
				label.setBackground(Color.RED);
			}
		}
}
```
---
When I clicked to booking the seat on GUI(View). It will execute code in Controller's mousePress().

* View >>> GUIHandler (Controller concrete) to send the user command.


```
public void mousePressed(MouseEvent e){
...
    if(guiHandler.bookSeat(seatNO)){ ... }

}
```
---
And it will call method in model. 

* Controller >>> Model to change model's data. (Seat's status).


```
cinema.bookSeat(seatNO);
``` 
---
And also the controller will ask the view to change their appearance.

* Controller >>> View to change label's background color from green to red.


```
label.setBackground(Color.RED);
```
---
The model notify the view.

* Model >>> View to tell I(Model) have changed.

    
```
setChanged();
notifyObservers("Done, your seat is " + seatNO);
```
---

>  From source code View and Model are not depend on each other but use the controller to connect each other.

---

###The another example for Model-View-Controller pattern in JAVA API is `JTable` and `DefaultTableModel`

```
1. String[] columnText = {"Firstname","Lastname","Nickname"};
2. DefaultTableModel tableModel = new DefaultTableModel(columnText,0);
3. JTable table = new JTable(tableModel);
```
In the line:3 'table' will be an observer of tableModel. We can know by
```
4. JTable tableFromModel = (JTable)(model.getTableModelListeners()[0]);
5. System.out.println(table == tableFromModel);
```
  * It return `true`. So we can obtain that `tableModel` has a `table` as observer (Listener) .  
 
###View >>> Controller >>> Model
So when we add the data to `tableModel` maybe by press some button.
```
6. String[] data1 = {"Johny","Walker","Blend"};
7. modelTable.addRow(data1);
```

###Model >>> View
In line:7 the `tableModel` will notify all observers. In this case is only `table`. So `table` will update appearance immediately.

---
##Reference

* [Stack Over Flow](http://stackoverflow.com/questions/5217611/the-mvc-pattern-and-swing)
* [BitBucket Design Pattern](https://bytebucket.org/skeoop/oop/raw/f385b2f444fa5f3f640e85ebe99532aa88c06107/patterns/Design-Patterns.pdf)

---
## [This is the example code without Model-View-Controller](https://bitbucket.org/oop2final/model-view-controller-pattern/src/4aacba72564d981c5534db4ac33bc358629e941c/src/WithNoPattern)

* Exercise : Please rewrite by using Model-View-Controller