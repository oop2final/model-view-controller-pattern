package WithPattern;

/**
 * Controller
 * @author TAWEERAT CHAIMAN 5710546259
 * @author PATINYA YONGYAI 5710547204
 *
 */
public class Controller implements GUIHandler{
	private Model cinema;
	private GUI gui;
	public Controller(Model cinema, GUI gui){
		this.cinema = cinema;
		this.gui = gui;
		this.gui.setGUIHandler(this);
	}
	
	@Override
	public boolean bookSeat(String seatNO) {
		return cinema.bookSeat(seatNO);
	}
	
}
