package WithPattern;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

/**
 * View
 * @author TAREERAT CHAIMAN 5710546259 
 * @author PATINYA YONGYAI 5710547204
 */
public class GUI implements Observer{
	private JFrame frame;
	private List<JLabel> seatLabels = new ArrayList<JLabel>();
	private GUIHandler guiHandler;
	public GUI(){
		initComponents();
	}
	
	public void initComponents(){
		frame = new JFrame();
		frame.setTitle("With Model-View-Controller Pattern");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel main = new JPanel(new GridLayout(15,12,10,10));
		for(int i=65;i<80;i++){
			for(int j=1;j<13;j++){
				JLabel label = new JLabel((char)i + String.format("%02d", j));
				BookingEvent event = new BookingEvent(label);
				seatLabels.add(label);
				label.addMouseListener(event);
				label.setOpaque(true);
				label.setHorizontalAlignment(SwingConstants.CENTER);
				label.setBackground(Color.green);
				main.add(label);
			}
		}
		frame.add(main);
		frame.setVisible(true);
		frame.setSize(650, 650);
	}
	
	class BookingEvent extends MouseAdapter{
		private JLabel label;
		public BookingEvent(JLabel label){
			this.label = label;
		}
		
		public void mousePressed(MouseEvent e){
			String seatNO = label.getText();
			if(guiHandler.bookSeat(seatNO)){
				label.setBackground(Color.RED);
			}
		}
	}
	
	public List<JLabel> getSeatLabels(){
		return this.seatLabels;
	}

	@Override
	public void update(Observable o, Object arg) {
		if(arg == null) return;
		JOptionPane.showMessageDialog(frame,(String)arg);
	}
	
	public void setGUIHandler(GUIHandler guiHandler){
		this.guiHandler = guiHandler;
	}
}
