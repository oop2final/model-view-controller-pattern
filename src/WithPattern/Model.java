package WithPattern;
import java.util.Observable;

/**
 * Model interface.
 * @author TAWEERAT CHAIMAN 5710546259
 * @author PATINYA YONGYAI 5710547204
 */
public abstract class Model extends Observable{
	abstract boolean checkAvailableSeat(String seatNO);
	abstract boolean bookSeat(String seatNO);
}
