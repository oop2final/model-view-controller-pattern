package WithPattern;

/**
 * Main class.
 * @author TAREERAT CHAIMAN 5710546259 
 * @author PATINYA YONGYAI 5710547204
 */
public class Main {
	public static void main(String[] args) {
		Model cinemaModel = new Cinema();
		GUI gui = new GUI();
		cinemaModel.addObserver(gui);
		Controller controller = new Controller(cinemaModel,gui);
	}
}
