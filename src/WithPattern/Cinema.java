package WithPattern;
import java.util.HashMap;
import java.util.Map;

/**
 * Model
 * @author TAREERAT CHAIMAN 5710546259 
 * @author PATINYA YONGYAI 5710547204
 */
public class Cinema extends Model{
	private Map<String,Boolean> seatMap;
	
	public Cinema(){
		seatMap = setSeat();
	}
	
	public Map<String,Boolean> setSeat(){
		seatMap = new HashMap<String,Boolean>();
		for(int i=65;i<80;i++){
			for(int j=1;j<16;j++){
				String key = (char)i+String.format("%02d", j);
				seatMap.put(key, true);
			}
		}
		return seatMap;
	}
	
	public boolean checkAvailableSeat(String seatNO){
		boolean check = seatMap.get(seatNO);
		return check;
	}
	
	public boolean bookSeat(String seatNO){
		if(checkAvailableSeat(seatNO)){
			seatMap.replace(seatNO, false);
			setChanged();
			notifyObservers("Done, your seat is " + seatNO);
			return true;
		}
		return false;
	}
	
}
