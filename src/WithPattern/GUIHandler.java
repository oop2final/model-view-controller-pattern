package WithPattern;
/**
 * GUI handler (to handler command from View).
 * @author TAWEERAT CHAIMAN 5710546259
 * @author PATINYA YONGYAI 5710547204
 */
public interface GUIHandler {
	boolean bookSeat(String seatNO);
}
